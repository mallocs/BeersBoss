// app.js

const http = require('http');

// Create an instance of the http server to handle HTTP requests
let app = http.createServer((req, res) => {
    // Set a response type of plain text for the response
    res.writeHead(200, {'Content-Type': 'text/plain'});
    console.log(req.method + ' ' + req.url);
    console.log(req.body)
      req.on('data', chunk => {
    console.log(`Data chunk available: ${chunk}`)
  })
});

// Start the server on port 3000
app.listen(3000, '127.0.0.1');
console.log('Node server running on port 3000');

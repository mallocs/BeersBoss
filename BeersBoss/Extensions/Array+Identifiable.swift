//
//  Array+Identifiable.swift
//  BeersBoss
//

import Foundation


extension Array where Element: Identifiable {
    func firstIndex(matching: Element) -> Int? {
        for index in 0..<self.count {
            if self[index].id == matching.id {
                return index
            }
        }
        return nil
    }

    func firstIndex(matchingId: UUID) -> Int? {
        for index in 0..<self.count {
            if self[index].id as! UUID == matchingId {
                return index
            }
        }
        return nil
    }
}


//
//  String+Substring.swift
//  BeersBoss
//
//  Created by mallocs.net on 6/21/21.
//

import Foundation

// Taken from https://stackoverflow.com/a/26775912
extension String {
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
}

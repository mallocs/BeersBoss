//
//  String+ConvertUPCtoUPCA.swift
//  BeersBoss
//
//  Created by mallocs.net on 6/21/21.
//

import Foundation

extension String {
    func convertUPCToUPCA() -> String? {
        // Takes a string and tries to convert it to the 12 digit UPC-a used internally.
        // Mostly for UPC-e to UPC-a.
        // TODO: Doesn't handle EAN13. Probably should.
        guard self.count == 8 || self.count == 12 else {
            return nil
        }
        if self.count == 12 {
            return self
        }
        switch self[6] {
        case "0","1","2":
            return self[0..<3] + self[6] + "0000" + self[3..<6] + self[7]

        case "3":
            return self[0..<4] + "00000" + self[4..<6] + self[7]
        case "4":
            return self[0..<5] + "00000" + self[5] + self[7]
        default:
            return self[0..<6] + "0000" + self[6] + self[7]
        }
    }
}

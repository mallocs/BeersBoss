#!/usr/bin/env python3

import csv
import json
import urllib.request
import urllib.parse
import os.path
from time import sleep
from cairosvg import svg2png # TODO: should use virtualenv or something
 
def make_json_array(csvFilePath, duplicateAsId=None, intFields=[], arrayFields=[]):
    data = []
    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
        for row in csvReader:
            for field in intFields:
                if row[field] != "":
                    row[field] = int(row[field])
            for field in arrayFields:
                if row[field] == "":
                    continue
                elif row[field][:1] == "[" and row[field][-1:] == "]":  # a little magic so these array fields don't get too cluttered
                    row[field] = eval(row[field])
                else:
                    row[field] = eval(str("[" + row[field] + "]"))
            if duplicateAsId:
                row['id'] = row[duplicateAsId] # since Identifiable requires .id
            for k,v in row.items():
                if v == "TRUE":
                    row[k] = True
                elif v == "FALSE":
                    row[k] = False
            row = {k: v for k, v in row.items() if v != ""} # remove keys with empty string values. Maybe DictReader could just skip empty fields? That would be better.
            if len(row) != 0:
                data.append(row)
        return data
            

def writeJson(data, jsonFilePath):
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
        jsonf.write(json.dumps(data, indent=4))
        

def getExt(path):
    return os.path.splitext(path)[1]

# Note: Assumes svgs are converted to png
def makeBBLogoFilename(entry, logoUrlKey, filenameKey):
    url = entry[logoUrlKey]
    ext = getExt(url)
    if ext == '.svg':
        ext = '.png'
    return entry[filenameKey].replace(" ", "_")+ext


def download_logos(csvFilePath, logoUrlKey, filenameKey, directory="."):
    data = []
    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
        for row in csvReader:
            url = row[logoUrlKey]
            if url == '':
                continue
            completeName = os.path.join(directory, makeBBLogoFilename(row, logoUrlKey, filenameKey))
            if getExt(url) == ".svg":
                completeName = completeName[:-3] + "png"
            if os.path.isfile(completeName):
                continue
            print("downloading: " + url)
            with urllib.request.urlopen(urllib.request.Request(
                url,
                data=None,
                headers={
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
                })) as response, open(completeName, 'wb') as out_file:
                print("writing to: " + completeName)
                data = response.read() # a `bytes` object
                if getExt(url) == ".svg":
                    svg2png(bytestring=data,write_to=completeName)
                else:
                    out_file.write(data)
                sleep(5)


def useBBLogoUrls(data, logoUrlKey, filenameKey, bbDirectory=""):
    for entry in data:
        if logoUrlKey in entry:
            entry[logoUrlKey] = "https://beersboss.com/" + urllib.parse.quote(bbDirectory + makeBBLogoFilename(entry, logoUrlKey, filenameKey))
    return data


def getDuplicates(jsonFilePath, checkField):
    with open(jsonFilePath, 'r', encoding='utf-8') as jsonf:
        data = json.load(jsonf)
        duplicates = []
        totalSet = set()
        for item in data:
            if not checkField in item:
                continue
            field = item[checkField]
            fieldSet = set([field]) if type(field) == str else set(field)
            duplicates += totalSet.intersection(fieldSet)
            totalSet.update(fieldSet)
        return duplicates
            

download_logos(r'entities.csv', r'logoUrl', r'name', r'/Users/vegas/dev/BeersBossWeb/assets/logos')

writeJson(
    useBBLogoUrls(
        make_json_array(r'entities.csv', r'id', ['id'], ['groupMembers', 'owners']),
        r'logoUrl',
        r'name',
        r'assets/logos/'
    ),
    r'entities.json'
)

writeJson(
    make_json_array(r'upcs.csv', intFields=['internalId']),
    r'upcs.json'
)


duplicates = getDuplicates(r'upcs.json', r'id')
print("No duplicates" if duplicates == [] else "upcs.json has duplicates: " + str(duplicates))

//
//  Entity.swift
//  BeersBoss
//
//  Created by mallocs.net on 4/21/21.
//
// id,name,owners,groupMembers,multipleOwners,logoUrl,wikiUrl,officialUrl

import Foundation

struct Entity: Hashable, Codable, Identifiable {
    let id: Int
    let name: String
    let owners: Array<Int>?
    let groupMembers: Array<Int>?
    let logoUrl: URL?
    let wikiUrl: URL?
    let officialUrl: URL?
}

//
//  GoogleForm.swift
//  BeersBoss
//
//  Created by mallocs.net on 4/26/21.
//

import Foundation

struct GoogleForm: Codable, Identifiable {
    let id: URL // This is the URL to post the form to
    let data: [String:String]
    let optionalData: [String: String]?
}

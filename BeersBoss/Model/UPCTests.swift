//
//  UPCTests.swift
//  BeersBossTests
//
//  Created by mallocs.net on 6/19/21.
//

import XCTest
@testable import BeersBoss

class UPCTests: XCTestCase {

    func testUPCToUPCAgivenUPCA() throws {
        let upc = "083820359626"
        let result = upc.convertUPCToUPCA()
        XCTAssertEqual(upc, result, "UPC should be the same after conversion")
    }
    
    func testUPCToUPCAgivenNonUPC() throws {
        let upc = "083820359626asfaf"
        let result = upc.convertUPCToUPCA()
        XCTAssertEqual(nil, result, "should return nil")
    }
    
    func testUPCToUPCAgivenUPCEcase1() throws {
        // upc[6] should be 0,1, or 2
        let upc = "0abcde18"
        let expectedResult = "0ab10000cde8"
        let result = upc.convertUPCToUPCA()
        XCTAssertEqual(expectedResult, result, "should be a upc-a after conversion")
    }

    func testUPCToUPCAgivenUPCEcase2() throws {
        let upc = "0abcde38"
        let expectedResult = "0abc00000de8"
        let result = upc.convertUPCToUPCA()
        XCTAssertEqual(expectedResult, result, "should be a upc-a after conversion")
    }

    func testUPCToUPCAgivenUPCEcase3() throws {
        let upc = "0abcde48"
        let expectedResult = "0abcd00000e8"
        let result = upc.convertUPCToUPCA()
        XCTAssertEqual(expectedResult, result, "should be a upc-a after conversion")
    }
    
    func testUPCToUPCAgivenUPCEcase4() throws {
        // upc[6] should be 5,6,7,8, or 9
        let upc = "0abcde58"
        let expectedResult = "0abcde000058"
        let result = upc.convertUPCToUPCA()
        XCTAssertEqual(expectedResult, result, "should be a upc-a after conversion")
    }
}

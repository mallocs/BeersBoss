//
//  UPC.swift
//  BeersBoss
//
//  Created by mallocs.net on 4/7/21.
//

import Foundation

struct UPC: Codable, Identifiable {
    let id: String // The actual UPC code or prefix
    let internalId: Int // entity id
    init?(id: String, internalId: Int) {
        guard id.convertUPCToUPCA() != nil else {
            return nil
        }
        self.id = id.convertUPCToUPCA()!
        self.internalId = internalId
    }
}

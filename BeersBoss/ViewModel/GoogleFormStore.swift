//
//  GoogleFormRequest.swift
//  BeersBoss
//
//  Created by mallocs.net on 4/27/21.
//

import Foundation

enum SubmissionError: Error {
    case validation
    case networkTimeout
    case server
}

typealias SubmitCompletionHandler = (String?) -> Void

typealias SubmitErrorHandler = (SubmissionError, String?) -> Void

class GoogleFormStore : ObservableObject {
    @Published private (set) var form: GoogleForm

    init(_ sheetUrl: URL, data: [String: String], optionalData: [String: String]?) {
        form = GoogleForm(id: sheetUrl, data: data, optionalData: optionalData)
    }
    
    func validate() -> Bool {
        return form.data.filter { $0.value == "" }.count == 0
    }
    
    func sendForm(_ handleSubmitCompletion: SubmitCompletionHandler? = nil, _ handleSubmitError: SubmitErrorHandler? = nil) {
        guard validate() else {
            if let handleSubmitError = handleSubmitError {
                handleSubmitError(SubmissionError.validation, "Validation Error")
            }
            return
        }
        var request = URLRequest(url: form.id)
        request.httpMethod = "POST"
         
        let allData = form.optionalData != nil ? form.data.merging(form.optionalData!) { (_, optional) in optional } : form.data
        let postData = allData.reduce("") { result, keyValueTuple in
            let cur = result == "" ? "\(keyValueTuple.key)=\(keyValueTuple.value)" : "&\(keyValueTuple.key)=\(keyValueTuple.value)"
            return result + cur
        }
        // TODO: Nicer encoding. Posting JSON to Google Form didn't immediately work.
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
          "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let session = URLSession(configuration: config)
        request.httpBody = postData.data(using: String.Encoding.utf8)
        // TODO: Use combine/dataTaskPublisher since this is quick and ugly
        // maybe https://www.avanderlee.com/swift/combine-error-handling/
        let task = session.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    if let handleSubmitError = handleSubmitError {
                        handleSubmitError(SubmissionError.server, "Server error took place: \(error)")
                    }
                }
                if let httpResponse = response as? HTTPURLResponse,
                      !(200...299).contains(httpResponse.statusCode) {
                    if let handleSubmitError = handleSubmitError {
                        handleSubmitError(SubmissionError.server, "Server error took place: Code \(httpResponse.statusCode)")
                    }
                }

                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    if let handleSubmitCompletion = handleSubmitCompletion {
                        handleSubmitCompletion(dataString)
                    }
                }
        }
        task.resume()
    }
}

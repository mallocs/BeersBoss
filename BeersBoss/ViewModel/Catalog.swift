//
//  Catalog.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/6/21.
//

import Foundation
import Kingfisher

class ProductCatalog : ObservableObject {
    private let ENTITIES_JSON_URL = URL(string: "https://beersboss.com/data/entities.json")!
    private let UPCS_JSON_URL = URL(string: "https://beersboss.com/data/upcs.json")!
    
    @Published private (set) var entities: [Int: Entity] = [:]
    @Published private (set) var upcs: [String: UPC] = [:]

    init() {
        if let path = Bundle.main.path(forResource: "entities", ofType: "json") {
            let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            self.updateEntities(data)
        }
        // precache logos by checking for a corresponding asset based on the URL filename but cache
        // it based on the absolute URL.
        entities.forEach {
            guard let logoUrl = $1.logoUrl,
                  let image = UIImage(named: logoUrl.lastPathComponent.components(separatedBy: ".")[0])
            else {
                return
            }
            let cacheKey = logoUrl.absoluteString
            ImageCache.default.store(image, forKey: cacheKey)
        }
        
        if let path = Bundle.main.path(forResource: "upcs", ofType: "json") {
            let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            self.updateUpcs(data)
        }
    }
    
    private func handleClientError(_ error: Error, from source: String) {
        #if DEBUG
            print("client error from \(source): ")
            print(error)
        #else
            QL3("client error from \(source): \(error)")
        #endif
    }
    
    private func handleServerError(_ error: URLResponse?) {
        #if DEBUG
            print("server error: ")
            print(error ?? "unknown")
        #else
            QL3("server error: \(error != nil ? "\(String(describing: error))" : "unknown")")
        #endif
    }
    
    private func updateEntities(_ data: Data) {
        do {
            let entitiesArray: [Entity] = try JSONDecoder().decode([Entity].self, from: data)
            for entity in entitiesArray {
                entities[entity.id] = entity
            }
        } catch {
            handleClientError(error, from: #function)
        }
    }
    
    
    private func updateUpcs(_ data: Data) {
        do {
            let upcsArray: [UPC] = try JSONDecoder().decode([UPC].self, from: data)
            for upc in upcsArray {
                upcs[upc.id] = upc
            }
        } catch {
            handleClientError(error, from: #function)
        }
    }
    
    func update() {
        fetchData(url: ENTITIES_JSON_URL) { data in
            self.updateEntities(data)
        }
        fetchData(url: UPCS_JSON_URL) { data in
            self.updateUpcs(data)
        }
    }
    
    private func fetchData(url: URL, completionHandler: @escaping (_ data: Data) -> Void) {
      let task = URLSession.shared.dataTask(with: url) { data, response, error in
        if let error = error {
            self.handleClientError(error, from: #function)
            return
        }
        guard let httpResponse = response as? HTTPURLResponse,
            (200...299).contains(httpResponse.statusCode) else {
            self.handleServerError(response)
            return
        }
        if let mimeType = httpResponse.mimeType, mimeType == "application/json",
            let data = data {
            DispatchQueue.main.async {
                completionHandler(data)
            }
        }
      }
      task.resume()
    }
    
    func allBeers<T: Comparable>(by keyPath: KeyPath<Entity, T>, using comparator: (T, T) -> Bool = (<)) -> [Entity] {
        // assumes only using ids less than 1000 for beers, but move to a proper DB at some point.
        return entities.filter{ $0.value.id < 1000 }.map { $0.value }.sorted(by: keyPath, using: comparator)
    }
    
    func allCompanies<T: Comparable>(by keyPath: KeyPath<Entity, T>, using comparator: (T, T) -> Bool = (<)) -> [Entity] {
        return entities.filter{ $0.value.id >= 1000 && $0.value.id < 2000 }.map { $0.value }.sorted(by: keyPath, using: comparator)
    }
    
    
    func allBeersOwnedByCompany(withKey key: Int) -> Array<Entity> {
        guard self.entity(forKey: key) != nil else {
            return []
        }
        return self.allBeers(by: \.name).filter { $0.owners == nil ? false : $0.owners!.contains(key) }
    }
    
    func entities(forKeys keys: Array<Int>) -> Array<Entity>? {
        keys.compactMap { entities[$0] }
    }
    
    func entity(forKey key: Int) -> Entity? {
        if let data = entities[key] {
            return data
        }
        return nil
    }
    
    func entity(forUpc code: String) -> Entity? {
        guard let code = code.convertUPCToUPCA() else {
            return nil
        }
        // TODO: indicate prefix/exact match
        if let upc = upcs[code] {
            return entities[upc.internalId]
        }
        if let upcPrefixMatch = upcs[String(code.prefix(6))] {
            return entities[upcPrefixMatch.internalId]
        }
        return nil
    }
    

    func memberBrands(forEntity entity: Entity) -> Array<Entity>? {
        guard let group = self.entity(forKey: entity.id),
              let groupMembers = group.groupMembers else {
            return nil
        }
        return groupMembers.map {
            self.entity(forKey: $0)!
        }.sorted(by: \.name)
    }
}

// https://www.swiftbysundell.com/articles/sorting-swift-collections/
extension Sequence {
    func sorted<T: Comparable>(
        by keyPath: KeyPath<Element, T>,
        using comparator: (T, T) -> Bool = (<)
    ) -> [Element] {
        sorted { a, b in
            comparator(a[keyPath: keyPath], b[keyPath: keyPath])
        }
    }
}


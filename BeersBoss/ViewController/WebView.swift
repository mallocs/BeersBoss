//
//  WebView.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/13/21.
//

import Foundation
import WebKit
import SwiftUI

struct WebView: UIViewRepresentable {
    @Binding var url: URL

    func makeUIView(context: Context) -> WKWebView {
        let view = WKWebView(frame: .zero)
        view.load(URLRequest(url: url))
        return view
    }

    func updateUIView(_ view: WKWebView, context: UIViewRepresentableContext<WebView>) {
    }
}

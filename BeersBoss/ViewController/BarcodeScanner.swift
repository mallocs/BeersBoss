//
//  BarcodeScanner.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/3/21.
//

import SwiftUI
import UIKit
import BarcodeScanner
import AVFoundation

typealias BarcodeScannerHandler = (String) -> Void

struct BarcodeScanner: UIViewControllerRepresentable {
    @Binding var isPresented: Bool
    @Binding var isAuthorized: Bool?
    
    var handleBarcodeScanner: BarcodeScannerHandler
    
    func makeUIViewController(context: Context) -> BarcodeScannerViewController {
        let scanner = BarcodeScannerViewController()
        scanner.cameraViewController.barCodeFocusViewType = .twoDimensions
        scanner.codeDelegate = context.coordinator
        scanner.errorDelegate = context.coordinator
        scanner.dismissalDelegate = context.coordinator
        scanner.messageViewController.messages.scanningText = "Place a beer's barcode within the window to scan. The search will start automatically."
        return scanner
    }
    func updateUIViewController(_ uiViewController: BarcodeScannerViewController, context: Context) { }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self, handleBarcode: handleBarcodeScanner)
    }
    
    class Coordinator: NSObject, BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate, UINavigationControllerDelegate {
        var handleBarcode: BarcodeScannerHandler
        let parent: BarcodeScanner
        
        init(_ parent: BarcodeScanner, handleBarcode: @escaping BarcodeScannerHandler) {
            self.parent = parent
            self.handleBarcode = handleBarcode
            super.init()
            checkPermissions()
        }
        
        func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
            self.handleBarcode(code)
        }
        
        func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
            #if DEBUG
                print("barcode scanner error: \(error)")
            #else
                QL3("barcode scanner error: \(error)")
            #endif
        }
        
        func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
          controller.dismiss(animated: true, completion: nil)
        }
        
        private func checkPermissions() {
            let mediaType = AVMediaType.video
            let status = AVCaptureDevice.authorizationStatus(for: mediaType)

            switch status {
            case .denied, .restricted:
                parent.isAuthorized = false
            case.notDetermined:
                // Prompt the user for access.
                AVCaptureDevice.requestAccess(for: mediaType) { granted in
                    guard granted != true else { return }

                    // The UI must be updated on the main thread.
                    DispatchQueue.main.async {
                        self.parent.isAuthorized = false
                    }
                }

            default: break
            }
        }
    }
}

//
//  BarcodeScannerView.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/9/21.
//

import SwiftUI

struct BarcodeScannerView: View {
    @State private var isActive = false
    @State private var isAuthorized: Bool?
    @State private var upc: String?
    @State private var match: Entity?

    @EnvironmentObject var catalog: ProductCatalog

    var body: some View {
        NavigationView {
            if !isActive && isAuthorized != false {
                BarcodeScanner(isPresented: $isActive, isAuthorized: $isAuthorized, handleBarcodeScanner:  { code in
                    // catalog.entity(forUpc: "071990") Coors group beers
                    // catalog.beer(forUpc: "089826") Heineken Mexico group
                    match = catalog.entity(forUpc: String(code))
                    upc = code
                    isActive = true
                })
                .navigationBarHidden(true)
            }
            if isAuthorized == false {
                NotAuthorized()
            }
            NavigationLink(destination: ScannedBeerView(match: match, upc: upc, isShowing: $isActive), isActive: $isActive) {}
        }.navigationViewStyle(StackNavigationViewStyle()) // TODO: nice split view for ipad
    }
}

struct BarcodeScannerView_Previews: PreviewProvider {
    static var previews: some View {
        BarcodeScannerView().environmentObject(ProductCatalog())
    }
}

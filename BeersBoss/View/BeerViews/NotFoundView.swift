//
//  NotFound.swift
//  BeersBoss
//
//  Created by mallocs.net on 4/27/21.
//

import SwiftUI

struct NotFoundView: View {
    let upc: String
    @State private var isShowingSubmitForm = false
    @Binding var isShowing: Bool

    var body: some View {
        VStack {
            Spacer()
            Text("Couldn't find that UPC.")
                .multilineTextAlignment(.center)
            NavigationLink(
                destination: NewBeerFormView(upc: upc, isShowing: $isShowing),
                isActive: $isShowingSubmitForm,
                label: { EmptyView() }
            )
            .navigationBarTitle("UPC Not Found", displayMode: .inline)
            Button {
                self.isShowingSubmitForm = true
            } label: {
                Text("If it's something we should have, submit it and we'll try to track it down.")
                Image(systemName: "chevron.right")
            }
            .multilineTextAlignment(.center)
            .padding(10)
            ZStack {
                Image("6pack")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 250.0, height: 250.0)
                Text("?")
                    .padding(.top, 70)
                    .font(.system(size: 105))
            }
            Spacer()
        }
    }
}

struct NotFoundView_Previews: PreviewProvider {
    static var previews: some View {
        NotFoundView(upc: "972311630015", isShowing: .constant(true))
    }
}

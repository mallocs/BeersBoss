//
//  ScannedBeerView.swift
//  BeersBoss
//
//  Created by mallocs.net on 6/2/21.
//

import SwiftUI

struct ScannedBeerView: View {
    @EnvironmentObject var catalog: ProductCatalog
    let match: Entity?
    let upc: String?
    @Binding var isShowing: Bool

    var body: some View {
        VStack {
            if let match = match {
                BeerView(beer: match)
            } else if let upc = upc {
                NotFoundView(upc: upc, isShowing: $isShowing)
            }
            Spacer()
        }
    }
}

struct ScannedBeerView_Previews: PreviewProvider {
    static var previews: some View {
        ScannedBeerView(match: ProductCatalog().entity(forUpc: "786150000311"), upc: "786150000311", isShowing: .constant(true)).environmentObject(ProductCatalog())
        ScannedBeerView(match: ProductCatalog().entity(forUpc: "07199840"), upc: "07199840", isShowing: .constant(true)).environmentObject(ProductCatalog())
        ScannedBeerView(match: ProductCatalog().entity(forUpc: "08787337"), upc: "08787337", isShowing: .constant(true)).environmentObject(ProductCatalog())
    }
}



//
//  BeerView.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/9/21.
//

import SwiftUI
import Kingfisher


struct OwnerLink: View {
    let entity: Entity
    
    var body: some View {
        NavigationLink(destination: CompanyView(company: entity)) {
            Text(entity.name)
            Image(systemName: "chevron.right")
        }
    }
}


struct BeerView: View {
    @EnvironmentObject var catalog: ProductCatalog
    let beer: Entity
    
    var body: some View {
        GeometryReader { g in
            ScrollView {
                if let memberBrands = catalog.memberBrands(forEntity: beer) {
                    VStack {
                        HStack() {
                            Spacer()
                            Text("One of these brands:")
                            Spacer()
                        }
                        .font(.title)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                        .background(Color(UIColor.systemGray6))
                        List(memberBrands) { beer in
                            NavigationLink(destination: BeerView(beer: beer)) {
                                BeerRowView(beer: beer, showOwner: false)
                            }
                        }.navigationBarTitle("Brewery Group", displayMode: .inline)
                        .frame(width: g.size.width, height: CGFloat(memberBrands.count * 65), alignment: .center)
                    }
                } else {
                    Text("\(beer.name)")
                        .font(.largeTitle)
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.center)
                    .padding(.top, 10)
                    .padding(.bottom, 10)
                    if let logoUrl = beer.logoUrl {
                         KFImage(logoUrl)
                            .resizable()
                            .loadImmediately()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 220)
                            .padding(.bottom, 20)
                    }
                }
                HStack {
                    if let ownerKeys = beer.owners,
                       let owners = catalog.entities(forKeys: ownerKeys) {
                        Text("Owned by:")
                        if owners.count > 1 {
                            VStack {
                                ForEach(owners) { owner in
                                    OwnerLink(entity: owner)
                                }
                                Text("* depends on region").font(.caption)
                            }
                        } else {
                            OwnerLink(entity: owners[0])
                        }
                    } else {
                        Spacer()
                        Text("Is Independent")
                            .font(.title)
                        Spacer()
                    }
                    Spacer()
                }
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 0))
                .background(Color(UIColor.systemGray6))
                if let url = beer.officialUrl {
                    Link(destination: url) {
                        HStack {
                            Text("Visit \(beer.name) website")
                            Image(systemName: "chevron.right")
                            Spacer()
                        }
                        
                    }.font(.subheadline)
                    .padding(.leading, 20)
                }
                Divider()
                if let wikiUrl = beer.wikiUrl  {
                    WikiView(url: wikiUrl)
                        .frame(width: g.size.width, height: g.size.height, alignment: .center)
                }
            }
        }
    }
}





struct BeerView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
//        BeerView(beer: ProductCatalog().beer(forKey: 2)).environmentObject(ProductCatalog()) // Burgkopf
//        BeerView(beer: ProductCatalog().beer(forKey: 3)).environmentObject(ProductCatalog()) // Sierra Nevada
//        ScannedBeerView(beer: ProductCatalog().beer(forKey: 4)).environmentObject(ProductCatalog()) //  Red Strip
//            ScannedBeerView(match: ProductCatalog().entity(forKey: 36)).environmentObject(ProductCatalog()) //  Tecate
            ScannedBeerView(match: ProductCatalog().entity(forKey: 13), upc: "111111", isShowing: .constant(true)).environmentObject(ProductCatalog()) //  Blue Moon
//        BeerView(beer: nil).environmentObject(ProductCatalog())
        }//.previewLayout(.fixed(width: 300, height: 70))
    }
}


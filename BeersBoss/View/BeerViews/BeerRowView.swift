//
//  BeerRowView.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/13/21.
//

import SwiftUI
import Kingfisher

struct BeerRowView: View {
    var beer: Entity
    var showOwner = true
    @EnvironmentObject var catalog: ProductCatalog
    
    var body: some View {
        HStack {
            if let url = beer.logoUrl {
                 KFImage(url)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 50, height: 50)
            }
            VStack(alignment: .leading) {
                Text(beer.name)
                if showOwner == true && beer.owners != nil, let owners = catalog.entities(forKeys: beer.owners!) {
                    Text("-\(owners.map { $0.name }.joined(separator: ", "))")
                        .font(.caption)
                }
            }
            Spacer()
        }
    }
}

struct BeerRowView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            BeerRowView(beer: ProductCatalog().entity(forKey: 3)!).environmentObject(ProductCatalog()) // Sierra Nevada
            BeerRowView(beer: ProductCatalog().entity(forKey: 4)!).environmentObject(ProductCatalog()) //  Red Strip
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}

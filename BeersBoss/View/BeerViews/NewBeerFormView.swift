//
//  SwiftUIView.swift
//  BeersBoss
//
//  Created by mallocs.net on 4/27/21.
//

import Foundation
import SwiftUI

struct SubmitButton: View {
    var store: GoogleFormStore
    @Binding var disable: Bool
    let submitCompletionHandler: SubmitCompletionHandler?
    let submitErrorHandler: SubmitErrorHandler?


    var body: some View {
        Button(action: {
            // dismiss keyboard after click
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
            // disable submission while processing
            disable = true
            store.sendForm(submitCompletionHandler, submitErrorHandler)
        }) {
            HStack {
                Text("Submit")
                Image(systemName: "paperplane.circle")
                    .imageScale(.large)
                    .foregroundColor(foregroundColor)
            }
        }.padding(.vertical, 13)
        .frame(maxWidth: .infinity)
        .foregroundColor(foregroundColor)
        .background(backgroundColor)
        .font(.title3)
        .border(backgroundColor)
        .cornerRadius(12)
        .disabled(disable)
        .opacity(disable ? 0.5 : 1)
    }

    let fileURL: URL? = nil
    let foregroundColor = Color.white
    let backgroundColor = Color.blue
}

struct NewBeerFormView: View {
    @State var breweryName: String = ""
    @State var beerName: String = ""
    @State var hasValidationError = false
    @State var isUPCSubmitted = false
    @State var errorMessage = ""
    @State var disableSubmitButton = false

    let upc: String
    let deviceId: String = UIDevice.current.identifierForVendor?.uuidString ?? "Unknown Device"
    @Binding var isShowing: Bool

    var body: some View {

        // Doing this to re-enable the Submit button after a validation error has been fixed.
        // There's probably a better way.
        let breweryNameBinding = Binding<String>(
            get: {
                self.breweryName
        },
            set: {
                if hasValidationError && disableSubmitButton {
                    disableSubmitButton = false
                    hasValidationError = false
                }
                self.breweryName = $0
            }
        )
        
        NavigationView {
            Form {
                Section(header: Text("Brewery Name")) {
                    HStack(spacing: 0) {
                        if hasValidationError && breweryName == "" {
                            Text("*")
                                .foregroundColor(Color.red)
                                .font(.largeTitle)
                                .baselineOffset(-12)
                                .padding(.vertical, -25)
                        }
                        Spacer()
                        TextField("Enter brewery name", text: breweryNameBinding)
                        Spacer()
                    }
                }
                Section(header: Text("Beer Name")) {
                    HStack(spacing: 0) {
                        Spacer()
                        TextField("Enter beer name", text: $beerName)
                        Spacer()
                    }                }
                Section {
                    if isUPCSubmitted {
                        HStack {
                            Spacer()
                            VStack {
                                Text("Submission complete").padding(5)
                                Text("Thank You! 😃").padding(5)
                            }
                            Spacer()
                        }
                    }
                    if errorMessage != "" {
                        HStack {
                            Spacer()
                            VStack {
                                Text(errorMessage)
                                    .foregroundColor(Color.red)
                                    .padding(15)
                            }
                            Spacer()
                        }
                    }
                    VStack(spacing: 0) {
                        Spacer()
                        HStack {
                            Spacer()
                            if disableSubmitButton && breweryName == "" {
                                Text("*")
                                    .foregroundColor(Color.red)
                                    .font(.largeTitle)
                                    .baselineOffset(-12)
                                    .padding(.trailing, -7)
                                Text("Missing required field")
                                    .foregroundColor(Color.red)
                                    .font(.callout)
                            } else {
                                Text(" ")
                                    .foregroundColor(Color.red)
                                    .font(.largeTitle)
                                    .baselineOffset(-12)
                                    .padding(.trailing, -7)
                                Text(" ")
                                    .foregroundColor(Color.red)
                                    .font(.callout)
                            }
                            Spacer()
                        }
                        HStack {
                            Spacer()
                            SubmitButton(
                                store: GoogleFormStore(
//                                    URL(string: "http://127.0.0.1:3000/post")!,
                                        URL(string: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSc46SxcawNXhlXLkVF5fRymYxjjVRNehwJ_q-1hdb_Qy60fKA/formResponse")!,
                                data: [
                                    "entry.2119781762": upc,
                                    "entry.578305303": deviceId,
                                    "entry.461917894": "\(breweryName)"
                                ],
                                optionalData: ["entry.1878167826": "\(beerName)"]
                                ),
                                disable: $disableSubmitButton,
                                submitCompletionHandler: { result in
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                        self.isShowing = false
                                    }
                                    self.isUPCSubmitted = true
                                },
                                submitErrorHandler: { error,_ in
                                    disableSubmitButton = true
                                    if error == .validation {
                                        hasValidationError = true
                                        return
                                    }
                                    if error == .networkTimeout {
                                        errorMessage = "Can't connect to server. Please try again later."
                                        // TODO: Probably won't log because of the same network issue.
                                        // Should store the log and send when network is available again.
                                        QL3("New Beer Form network connection issue")
                                    }
                                    if error == .server {
                                        errorMessage = "Server error. Please try again later."
                                        QL3("New Beer Form server error")
                                    }
                                    // Force some pause between submission attempts
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                                        self.disableSubmitButton = false
                                    }
                                }
                            ).padding(.horizontal, 22)
                            Spacer()
                        }
                    }.padding(.top, -25)
                }
                .listRowInsets(EdgeInsets())
                .background(Color(UIColor.systemGray6))
                .textCase(nil)
            }.navigationBarTitle("Submit a UPC", displayMode: .inline)
            .padding(.top, 100)
            .background(Color(UIColor.systemGray6))
        }
    }
}

struct NewBeerFormView_Previews: PreviewProvider {
    static var previews: some View {
        NewBeerFormView(upc: "111111", isShowing: .constant(true))
    }
}

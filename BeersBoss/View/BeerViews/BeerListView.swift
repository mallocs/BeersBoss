//
//  BeerListView.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/13/21.
//

import SwiftUI

struct BeerListView: View {
    @EnvironmentObject var catalog: ProductCatalog
    var body: some View {
        NavigationView {
            List(catalog.allBeers(by: \.name)) { beer in
                NavigationLink(destination: BeerView(beer: beer)) {
                    BeerRowView(beer: beer)
                }
            }.navigationBarTitle("Brands", displayMode: .inline)
          //  SelectAVBeerView() // TODO: Needs to show primary and secondary at same time
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct SelectAVBeerView: View {
    var body: some View {
        VStack {
            Text("Welcome to Beers Boss")
                .font(.largeTitle)
            Text("Please select a beer from the left-hand menu; swipe from the left edge to show it.")
                .foregroundColor(.secondary)
        }
    }
}

struct BeerListView_Previews: PreviewProvider {
    static var previews: some View {
        BeerListView()
    }
}

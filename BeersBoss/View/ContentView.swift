//
//  ContentView.swift
//  BeersBoss
//
//  Created by mallocs.net on 2/28/21.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var catalog: ProductCatalog
    var body: some View {
        TabView {
            BarcodeScannerView()
                .tabItem {
                    Label("Scan", systemImage: "barcode.viewfinder")
            }
            BeerListView()
                .tabItem {
                    Label("Brands", systemImage: "magnifyingglass")
                }
            CompanyListView()
                .tabItem {
                    Label("Owners", systemImage: "building.2")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

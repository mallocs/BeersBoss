//
//  NotAuthorized.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/23/21.
//

import SwiftUI

struct NotAuthorized: View {
    @State var alertVisible = true
    var body: some View {
        VStack {
            HStack {
                Image(systemName: "camera")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 50, height: 30)
                    .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                Button(action: {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }) {
                    Text("Change settings")
                    .font(.title)
                }
            }
            Text("Beers Boss needs camera access to scan barcodes")
                .font(.body)
                .padding(10)
                .alert(isPresented: $alertVisible) {
                Alert (title: Text("Camera access is required to scan barcodes"),
                       message: Text("Go to Settings?"),
                       primaryButton: .default(Text("Settings"), action: {
                           UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                       }),
                       secondaryButton: .default(Text("Cancel")))
                   }
        }.padding(50)
    }
}

struct NotAuthorized_Previews: PreviewProvider {
    static var previews: some View {
        NotAuthorized()
    }
}

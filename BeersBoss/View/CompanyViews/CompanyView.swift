//
//  CompanyView.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/17/21.
//

import SwiftUI
import Kingfisher

struct CompanyView: View {
    @EnvironmentObject var catalog: ProductCatalog    
    let company: Entity
    
    var body: some View {
        GeometryReader { g in
            ScrollView {
                Text("\(company.name)").font(.largeTitle).fontWeight(.semibold).multilineTextAlignment(.center)
                .padding(.top, 10)
                .padding(.bottom, 10)
                if let url = company.logoUrl {
                     KFImage(url)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 220)
                        .padding(.bottom, 20)
                }
                HStack {
                    Spacer()
                    VStack {
                            Text("Owns brands including:").font(.title)
                    }
                    Spacer()
                }
                .padding(.top, 10)
                .padding(.bottom, 10)
                .background(Color(UIColor.systemGray6))
                    VStack {
                        List(catalog.allBeersOwnedByCompany(withKey: company.id)) { beer in
                            NavigationLink(destination: BeerView(beer: beer)) {
                                BeerRowView(beer: beer, showOwner: false)
                            }
                        }.navigationBarTitle("", displayMode: .inline)
                        .frame(width: g.size.width, height: CGFloat(catalog.allBeersOwnedByCompany(withKey: company.id).count) * 65, alignment: .center)
                    }
                if let wikiUrl = company.wikiUrl  {
                    WikiView(url: wikiUrl)
                        .frame(width: g.size.width, height: g.size.height, alignment: .center)
                }
            }
        }
    }
}


struct WikiView: View {
    @State var url: URL
    var body: some View {
        WebView(url: $url)
    }
}

struct CompanyView_Previews: PreviewProvider {
    static var previews: some View {
        CompanyView(company: ProductCatalog().entity(forKey: 1)!).environmentObject(ProductCatalog())
    }
}

//
//  CompanyListView.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/17/21.
//

import SwiftUI

struct CompanyListView: View {
    @EnvironmentObject var catalog: ProductCatalog
    
    var body: some View {
        NavigationView {
            List(catalog.allCompanies(by: \.name)) { company in
                NavigationLink(destination: CompanyView(company: company)) {
                    CompanyRowView(company: company)
                }
            }.navigationBarTitle("Owners", displayMode: .inline)
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct CompanyListView_Previews: PreviewProvider {
    static var previews: some View {
        CompanyListView()
    }
}

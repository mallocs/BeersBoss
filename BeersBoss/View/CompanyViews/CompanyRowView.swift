//
//  CompanyRowView.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/17/21.
//

import SwiftUI
import Kingfisher

struct CompanyRowView: View {
    var company: Entity

    var body: some View {
        HStack {
            if let url = company.logoUrl {
                 KFImage(url)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 50, height: 50)
            }
            Text(company.name)
            Spacer()
        }
    }
}

struct CompanyRowView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CompanyRowView(company: ProductCatalog().entity(forKey: 1000)!).environmentObject(ProductCatalog()) // Anheuser-Busch InBev
            CompanyRowView(company: ProductCatalog().entity(forKey: 1001)!).environmentObject(ProductCatalog()) //  Heineken
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}

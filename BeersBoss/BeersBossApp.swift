//
//  BeersBossApp.swift
//  BeersBoss
//
//  Created by mallocs.net on 3/2/21.
//

import SwiftUI

@main
struct BeersBossApp: App {
  //  @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @ObservedObject var catalog = ProductCatalog()
    
    init() {
        setupOnlineLogs()
        catalog.update()
    }

    private func setupOnlineLogs() {
        QorumOnlineLogs.setupOnlineLogs(formLink: "https://docs.google.com/forms/d/e/1FAIpQLScmk3cyVlX0dPSlGXoNIweks3y_2UpW6eficBjBjga9SPubQw/formResponse", versionField: "entry.1200291926", userInfoField: "entry.1670264255", methodInfoField: "entry.1219478421", textField: "entry.2052494645")
        QorumLogs.enabled = false
        QorumOnlineLogs.enabled = true
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(catalog)
        }
    }
}
